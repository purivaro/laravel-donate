@extends('layouts.main')

@section('title','Items')

@section('breadcrumb')
	<div class="row page-titles">
		<div class="col-md-5 align-self-center">
			<h3 class="text-themecolor">Items</h3>
		</div>
		<div class="col-md-7 align-self-center">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
				<li class="breadcrumb-item">pages</li>
				<li class="breadcrumb-item active">Items</li>
			</ol>
		</div>
	</div>
@endsection

@section('content')
	{{-- 
			<div class="row">
					<div class="col-12">
							<div class="card">
									<div class="card-body">
											<h1>Welcome {{ Auth::user()->name }} !!!</h1>
											<div class="text-right">
													<a href="/items/create" class="btn btn-success">New</a>
											</div>
											<table class="table mt-3">
													<thead>
															<tr>
																	<th>Todo</th>
																	<th>Detail</th>
																	<th>View</th>
																	<th>Edit</th>
																	<th>Delete</th>
															</tr>
													</thead>
													<tbody>
															@foreach($items as $item)
																	<tr>
																			<td>{{$item->title}}</td>
																			<td>{{$item->start_at}}</td>
																			<td><a href="/items/{{$item->id}}" class="btn btn-primary">View</a></td>
																			<td><a href="/items/{{$item->id}}/edit" class="btn btn-info">Edit</a></td>
																			<td><form action="/items/{{$item->id}}" method="post" >@csrf @method('DELETE') <button type="submit" class="btn btn-danger">Delete</button></form></td>
																	</tr>
															@endforeach
													</tbody>
											</table>
									</div>
							</div>
					</div>
			</div> 
	--}}

	{{-- <div class="row">
		<div class="col">
			<example-component></example-component>
			<v-btn small color="primary" dark>Small Button</v-btn>
			<v-btn color="warning" dark>Normal Button</v-btn>
		</div>
	</div> --}}

	<div class="row">
		<div class="col-12 col-sm">
			<div class="d-flex justify-content-between">
				<ul class="nav nav-pills " role="tablist">
					<li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home2" role="tab">กิจกรรม</a> </li>
					<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#profile2" role="tab">บุญทั่วไป</a> </li>
				</ul>
				<a href="" class="btn btn-success">New</a>
			</div>
			<!-- Tab panes -->
			<div class="tab-content">
				<div class="tab-pane pt-4 active" id="home2" role="tabpanel">
					<div class="row">
						<div class="col-12 col-sm">
							<div class="card">
								<div class="card-body">
									<div class="row">
										<div class="col-sm-3">
											<img src="https://www.dmc.tv/images/banners/600/00-BuJaKhawPhra.jpg" class="img-fluid" >
										</div>
										<div class="col-sm">
											<h4>บูชาข้าวพระ</h4>
											<p>3 ก.พ. 2562</p>
										</div>
										<div class="pr-3">
											<a href="#" class=""><i class="fas fa-ellipsis-v"></i></a>
										</div>
									</div>

								</div>
								<ul class="list-group list-group-flush">
									<li class="list-group-item">บุญถวายภัตตาหาร</li>
									<li class="list-group-item">บุญถวายผ้าอาบน้ำฝน</li>
									<li class="list-group-item">บุญคิลานเภสัช</li>
								</ul>
							</div>

							<div class="card">
								<div class="card-body">
									<div class="row">
										<div class="col-sm-3">
											<img src="http://www.tcatmall.com/upload/images/template-taobao.jpg" class="img-fluid" >
										</div>
										<div class="col-sm">
											<h4>มาฆบูชา</h4>
											<p>3 ก.พ. 2562</p>
										</div>
										<div class="pr-3">
												<a href="#" class=""><i class="fas fa-ellipsis-v"></i></a>
										</div>
									</div>

								</div>
								<ul class="list-group list-group-flush accordion">
									
									<li class="list-group-item d-flex justify-content-between position-relative" data-toggle="collapse" data-target="#item101">
										<span>บุญถวายภัตตาหาร</span>
										<div class=""><a href="#" class="stretched-link"><i class="fas fa-chevron-down"></i></a></div>
									</li>
									
									<div id="item101" class="collapse" >
										<div class="card-body">
											Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
										</div>
									</div>
									
									<li class="list-group-item d-flex justify-content-between position-relative" data-toggle="collapse" data-target="#item102">
										<span>บุญถวายโคมประทีป</span>
										<div class=""><a href="#" class="stretched-link"><i class="fas fa-chevron-down"></i></a></div>
									</li>
								
									<div id="item102" class="collapse" >
										<div class="card-body">
											Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
										</div>
									</div>
									<li class="list-group-item d-flex justify-content-between position-relative" data-toggle="collapse" data-target="#item103">
										<span>ทอดผ้าป่าปล่องไฟ</span>
										<div class=""><a href="#" class="stretched-link"><i class="fas fa-chevron-down"></i></a></div>
									</li>
									<div id="item103" class="collapse" >
										<div class="card-body">
											Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
										</div>
									</div>
								</ul>
							</div>								
						</div>

						<div class="col-12 col-sm-4">
							<div class="card">
								<div class="card-body">
									<h3>สรุป</h3>
								</div>
								
								<ul class="list-group list-group-flush">
									<li class="list-group-item d-flex justify-content-between">
										<div>บุญถวายภัตตาหาร</div>
										<div>50,000 บาท</div>
									</li>
									<li class="list-group-item d-flex justify-content-between">
										<div>บุญถวายโคมประทีป</div>
										<div>120,000 บาท</div>
									</li>
									<li class="list-group-item d-flex justify-content-between">
										<div>ทอดผ้าป่าปล่องไฟ</div>
										<div>8,000,000 บาท</div>
									</li>
								</ul>
							</div>
						</div>							
					</div>
				</div>
				<div class="tab-pane pt-4" id="profile2" role="tabpanel">
					<div class="row">
						<div class="col-12 col-sm">
							<div class="card">
								<div class="card-body">
									<div class="d-flex justify-content-between align-items-center">
										<h4>การศึกษา</h4>
										<a href="" class="btn btn-info">New</a>
									</div>
								</div>
							</div>								
						</div>
						<div class="col-12 col-sm-4"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection


