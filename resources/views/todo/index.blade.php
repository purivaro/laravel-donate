@extends('layouts.main')

@section('title','Todo')

@section('breadcrumb')
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Todo</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item">pages</li>
                <li class="breadcrumb-item active">Todo</li>
            </ol>
        </div>
        <div>
            <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
        </div>
    </div>
@endsection

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h1>Welcome {{ Auth::user()->name }} !!!</h1>
                    <div class="text-right">
                        <a href="/todo/create" class="btn btn-success">New</a>
                    </div>
                    <table class="table mt-3">
                        <thead>
                            <tr>
                                <th>Todo</th>
                                <th>Detail</th>
                                <th>View</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($todos as $todo)
                                <tr>
                                    <td>{{$todo->todo}}</td>
                                    <td>{{$todo->detail}}</td>
                                    <td><a href="/todo/{{$todo->id}}" class="btn btn-primary">View</a></td>
                                    <td><a href="/todo/{{$todo->id}}/edit" class="btn btn-info">Edit</a></td>
                                    <td><form action="/todo/{{$todo->id}}" method="post" >@csrf @method('DELETE') <button type="submit" class="btn btn-danger">Delete</button></form></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection


