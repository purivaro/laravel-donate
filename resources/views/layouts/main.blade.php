<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('/images/favicon.png')}}">
    <title>@yield('title')</title>
    <link href="{{ mix('/css/admin-press.bundle.css') }}" rel="stylesheet">
    {{-- <link href="{{ mix('/css/vuetify.css') }}" rel="stylesheet"> --}}
		@yield('css')
</head>
<body class="fix-header card-no-border">
    <div id="app">
		</div>
    <div id="">

        @include('layouts.preloader')

        <div id="main-wrapper">

            @include('layouts.topbar')

            @include('layouts.sidebar')

            <div class="page-wrapper">

                @yield('breadcrumb')

                <div class="container-fluid">
                    @yield('content')
                </div>

                @include('layouts.footer')
            </div>
        </div>
    </div>
    <script src="{{ mix('/js/admin-press.bundle.js') }}"></script>
    @yield('javascript')
</body>
</html>
