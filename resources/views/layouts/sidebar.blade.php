<aside class="left-sidebar">
	<!-- Sidebar scroll-->
	<div class="scroll-sidebar">
		<!-- User profile -->
		<div class="user-profile">
			<!-- User profile image -->
			<div class="profile-img"> <img src="{{asset('/images/users/profile.png')}}" alt="user" />
				<!-- this is blinking heartbit-->
				<div class="notify setpos"> <span class="heartbit"></span> <span class="point"></span> </div>
			</div>
			<!-- User profile text-->
			<div class="profile-text">
				<h5>{{ Auth::user()->name }}</h5>
			</div>
		</div>
		<!-- End User profile text-->
		<!-- Sidebar navigation-->
		<nav class="sidebar-nav">
			<ul id="sidebarnav">
				<li class="nav-devider"></li>
				<li> <a class="has-arrow waves-effect waves-dark" href="-" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Dashboard <span class="label label-rouded label-themecolor pull-right">4</span></span></a>
					<ul aria-expanded="false" class="collapse">
						<li><a href="/home">Home </a></li>
						<li><a href="/todo">Todo</a></li>
					</ul>
				</li>
				<li> <a class="has-arrow waves-effect waves-dark" href="-" aria-expanded="false"><i class="mdi mdi-bullseye"></i><span class="hide-menu">Donate</span></a>
					<ul aria-expanded="false" class="collapse">
						<li><a href="/activities">Activities</a></li>
						<li><a href="/items">Items</a></li>
						<li><a href="/activities">Activities</a></li>
						<li><a href="/donate">Donate</a></li>
					</ul>
				</li>
			</ul>
		</nav>
		<!-- End Sidebar navigation -->
	</div>
	<!-- End Sidebar scroll-->
</aside>
