<script>
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
	
	const vm1 = new Vue({
		el: "#vue-content",
		data: function() {
			return {
				activities: []
			}
		},
		created: function(){
			var self = this;
			
			self.loadData()


			$('body').on('submit','#form-activity',function(e){
				e.preventDefault()
				let form = $(this)
				let id = form.find('[name=id]').val()
				let url = form.attr('action')
				if(id){
					form.find('[name=_method]').val('PATCH')
					url = url+'/'+id
				}
				let data = new FormData(this);
				$.ajax({
					url: url,
					method: form.attr('method'),
					data: data,
					dataType: 'json',
					processData: false,
					contentType: false,
					cache: false,
					success: function(res){
						console.log(res)
						$('#newActivityModal').modal('hide')
						self.loadData()
					}
				})
			})

		},
		methods: {

			loadData: function(){
				var self = this;			
				$.ajax({
					url: '/activities/list',
					dataType: 'json',
					success: function(res) {
						self.activities = res
					}
				})
			},


		}
	})

	$(function(){
		

		// $('#newActivityModal').modal()
		$('body').on('click','.btn-newActivityModal',function(e){
			e.preventDefault();
			$('#newActivityModal').find('input,select').val('')
			$('#newActivityModal').find('[name=_method]').val('POST')
			$('#newActivityModal').modal('show')
		})

		$('body').on('click','.btn-edit-item',function(e){
			e.preventDefault();
			let id = $(this).attr('act_id')
			let url = `/activities/${id}`
			$.ajax({
				url: url,
				method: 'get',
				dataType: 'json',
				success: function(res){
					console.log(res)
					$('#newActivityModal').find('[name=id]').val(res.id)
					$('#newActivityModal').find('[name=title]').val(res.title)
					$('#newActivityModal').find('[name=start_at]').val(res.start_at)
					$('#newActivityModal').find('[name=end_at]').val(res.end_at)
					$('#newActivityModal').find('[name=img_cover_old]').val(res.img_cover)
					$('#newActivityModal').find('[name=note]').val(res.note)
					$('#newActivityModal').modal('show')
				}
			})
		})

		$('body').on('click','.btn-delete-item',function(e){
			e.preventDefault();
			$(this).next('form').submit();
		})

		$('body').on('submit','.form-del-act',function(e){
			e.preventDefault();
			let this_ = $(this);
			$.ajax({
				url: this_.attr('action'),
				method: this_.attr('method'),
				data: this_.serialize(),
				dataType: 'json',
				success: function(res) {
					console.log(res)
					if(res){
						this_.closest('.card').remove()
					}
				}
			})
		})


	})


</script>