@extends('layouts.main')

@section('title','Activity')

@section('css')
	<style>
		.dropdown-toggle::after{
			display:none;
		}
	</style>
@endsection

@section('breadcrumb')
	<div class="row page-titles">
		<div class="col-md-5 align-self-center">
			<h3 class="text-themecolor">Activity</h3>
		</div>
		<div class="col-md-7 align-self-center">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
				<li class="breadcrumb-item">pages</li>
				<li class="breadcrumb-item active">Activity</li>
			</ol>
		</div>
	</div>
@endsection

@section('content')

	<div class="row" id="vue-content">
		<div class="col-12 col-sm">
			<div class="d-flex justify-content-between">
				<ul class="nav nav-pills " role="tablist">
					<li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#tab-activity" role="tab">กิจกรรม</a> </li>
					<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#tab-general" role="tab">บุญทั่วไป</a> </li>
				</ul>
				<!-- Button trigger modal -->
				<button type="button" class="btn btn-success btn-newActivityModal" >New</button>
			</div>
			<!-- Tab panes -->
			<div class="tab-content" id="">

				<div class="tab-pane pt-4 active" id="tab-activity" role="tabpanel">
						
					<div class="row">
						<div class="col-12 col-sm">

							<div class="card" v-for="activity in activities">
								<div class="card-body">
									<div class="row">
										<div class="col-sm-3">
											<img :src="activity.img_cover" class="img-fluid" >
										</div>
										<div class="col-sm">
											<h4>@{{activity.title}}</h4>
											<p>@{{activity.start_at}}</p>
										</div>
										<div class="pr-3">
												{{-- <a href="#" class=""><i class="fas fa-ellipsis-v"></i></a> --}}

												<div class="btn-group">
													<a href="javascipt:;" class="dropdown-toggle no-caret px-2 text-muted" data-toggle="dropdown" aria-haspopup="false" aria-expanded="false">
														<i class='fas fa-ellipsis-v'></i>
													</a>
													<div class="dropdown-menu">
														<a href='javascipt:;' class="dropdown-item btn-edit-item" :act_id='activity.id' >แก้ไข</a>
														<a href='javascipt:;' class="dropdown-item btn-delete-item"  :act_id='activity.id' >ลบ</a>
														<form :action="'/activities/'+activity.id" method="post" class="form-del-act">
															@csrf
															@method('DELETE')
														</form>
													</div>
												</div>

										</div>
									</div>

								</div>
								<ul class="list-group list-group-flush accordion">
									
									<li class="list-group-item d-flex justify-content-between position-relative" data-toggle="collapse" :data-target="'#item'+activity.title+'-101'">
										<span>บุญถวายภัตตาหาร</span>
										<div class=""><a href="#" class="stretched-link"><i class="fas fa-chevron-down"></i></a></div>
									</li>
									
									<div :id="'item'+activity.title+'-101'" class="collapse" >
										<div class="card-body">
											Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
										</div>
									</div>
									
									<li class="list-group-item d-flex justify-content-between position-relative" data-toggle="collapse" :data-target="'#item'+activity.title+'-102'">
										<span>บุญถวายโคมประทีป</span>
										<div class=""><a href="#" class="stretched-link"><i class="fas fa-chevron-down"></i></a></div>
									</li>
								
									<div :id="'item'+activity.title+'-102'" class="collapse" >
										<div class="card-body">
											Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
										</div>
									</div>
								</ul>
							</div>								
							
							@foreach($activities as $activity)
								<div class="card">
									<div class="card-body">
										<div class="row">
											<div class="col-sm-3">
												<img src="{{$activity->img_cover}}" class="img-fluid" >
											</div>
											<div class="col-sm">
												<h4>{{$activity->title}}</h4>
												<p>{{$activity->start_at}}</p>
											</div>
											<div class="pr-3">
													{{-- <a href="#" class=""><i class="fas fa-ellipsis-v"></i></a> --}}

													<div class="btn-group">
														<a href="javascipt:;" class="dropdown-toggle no-caret px-2 text-muted" data-toggle="dropdown" aria-haspopup="false" aria-expanded="false">
															<i class='fas fa-ellipsis-v'></i>
														</a>
														<div class="dropdown-menu">
															<a href='javascipt:;' class="dropdown-item btn-edit-item" id='{{$activity->id}}' >แก้ไข</a>
															<a href='javascipt:;' class="dropdown-item btn-delete-item"  id='{{$activity->id}}' >ลบ</a>
														</div>
													</div>

											</div>
										</div>

									</div>
									<ul class="list-group list-group-flush accordion">
										
										<li class="list-group-item d-flex justify-content-between position-relative" data-toggle="collapse" data-target="#item{{$activity->title}}-101">
											<span>บุญถวายภัตตาหาร</span>
											<div class=""><a href="#" class="stretched-link"><i class="fas fa-chevron-down"></i></a></div>
										</li>
										
										<div id="item{{$activity->title}}-101" class="collapse" >
											<div class="card-body">
												Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
											</div>
										</div>
										
										<li class="list-group-item d-flex justify-content-between position-relative" data-toggle="collapse" data-target="#item{{$activity->title}}-102">
											<span>บุญถวายโคมประทีป</span>
											<div class=""><a href="#" class="stretched-link"><i class="fas fa-chevron-down"></i></a></div>
										</li>
									
										<div id="item{{$activity->title}}-102" class="collapse" >
											<div class="card-body">
												Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
											</div>
										</div>
										<li class="list-group-item d-flex justify-content-between position-relative" data-toggle="collapse" data-target="#item{{$activity->title}}-103">
											<span>ทอดผ้าป่าปล่องไฟ</span>
											<div class=""><a href="#" class="stretched-link"><i class="fas fa-chevron-down"></i></a></div>
										</li>
										<div id="item{{$activity->title}}-103" class="collapse" >
											<div class="card-body">
												Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
											</div>
										</div>
									</ul>
								</div>
							@endforeach

						</div>

						<div class="col-12 col-sm-4">
							<div class="card">
								<div class="card-body">
									<h3>สรุปประจำเดือน</h3>
								</div>
								
								<ul class="list-group list-group-flush">
									<li class="list-group-item d-flex justify-content-between">
										<div>มกราคม</div>
										<div>50,000 บาท</div>
									</li>
									<li class="list-group-item d-flex justify-content-between">
										<div>กุมภาพันธ์</div>
										<div>120,000 บาท</div>
									</li>
									<li class="list-group-item d-flex justify-content-between">
										<div>มีนาคม</div>
										<div>8,000,000 บาท</div>
									</li>
								</ul>
							</div>
						</div>							
					</div>
				</div>
				


				<div class="tab-pane pt-4" id="tab-general" role="tabpanel">
					<div class="row">
						<div class="col-12 col-sm">
							<div class="card">
								<div class="card-body">
									<div class="d-flex justify-content-between align-items-center">
										<h4>การศึกษา</h4>
										<a href="" class="btn btn-info">New</a>
									</div>
								</div>
							</div>								
						</div>
						<div class="col-12 col-sm-4"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	
	<!-- Modal -->
	<div class="modal fade" id="newActivityModal" tabindex="-1" role="dialog"  aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form action="/activities" method="POST" enctype="multipart/form-data" id="form-activity">
				@csrf
				@method('POST')
				<input type="hidden" name="id" class="form-control" value="">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">เพิ่มกิจกรรมใหม่</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>ชื่อกิจกรรม</label>
						<input type="text" name="title" class="form-control" value="">
					</div>
					<div class="form-group">
						<label>วันที่เริ่ม</label>
						<input type="text" name="start_at" class="form-control" value="">
					</div>
					<div class="form-group">
						<label>วันที่สิ้นสุด</label>
						<input type="text" name="end_at" class="form-control" value="">
					</div>
					<div class="form-group">
						<label>Cover Image</label>
						<input type="file" name="img_cover" class="form-control" value="">
						<input type="hidden" name="img_cover_old" value="">
					</div>
					<div class="form-group">
						<label>Note</label>
						<input type="text" name="note" class="form-control" value="">
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">บันทึก</button>
				</div>
			</form>
			</div>
		</div>
	</div>

@endsection


@section('javascript')
	@include('activities.js.script')
@endsection