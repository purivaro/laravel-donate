<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivityTagTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity_tag', function (Blueprint $table) {
            $table->primary(['activity_id', 'tag_id']);
            $table->unsignedInteger('activity_id');
						$table->unsignedInteger('tag_id');
						$table->foreign('activity_id')->references('id')->on('activities')->onDelete('cascade');
						$table->foreign('tag_id')->references('id')->on('tags')->onDelete('cascade');
            $table->timestamps();
				});

        // Schema::table('activity_tag', function (Blueprint $table) {
				// });
				


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activity_tag');
    }
}
