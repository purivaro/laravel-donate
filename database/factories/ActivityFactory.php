<?php

use Faker\Generator as Faker;

$factory->define(App\Activity::class, function (Faker $faker) {
    return [
			'owner_id'=> function() {
				return factory(App\User::class)->create()->id;
			},
      'title' => $faker->sentence,
      'start_at' => $faker->date($format = 'Y-m-d', $max = 'now'),
			'end_at' => $faker->date($format = 'Y-m-d'),
			'img_cover' => $faker->url,
			'note' => $faker->sentence,
    ];
});
