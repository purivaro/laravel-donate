<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\Filters\QueryFilter;

class Activity extends Model
{
	protected $fillable = ['title', 'start_at', 'end_at', 'img_cover', 'note', 'owner_id'];
	
	// protected $guarded = [];
	
	public function user() {
		return $this->belongsTo(User::class, 'owner_id');
	}

	public function tags() {
		return $this->belongsToMany(Tag::class)->withTimestamps();
	}

	public function scopeFilter($query, QueryFilter $filters)
	{
		return $filters->apply($query);
	}
}
