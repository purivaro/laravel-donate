<?php
namespace App\Http\Controllers\Filters;

class ActivityFilters extends QueryFilter
{
	public function title($title='')
	{
		return $this->builder->where('title','LIKE', '%'.$title.'%');
	}	

	public function start_at($start_at)
	{
		return $this->builder->where('start_at', $start_at);
	}	

	public function newest($order = 'desc')
	{
		return $this->builder->orderBy('start_at', $order);
	}	


}
?>