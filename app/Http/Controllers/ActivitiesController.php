<?php

namespace App\Http\Controllers;

use App\Activity;
use App\Http\Controllers\Filters\ActivityFilters;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use claviska\SimpleImage;
use Illuminate\Support\Facades\Storage;

class ActivitiesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $activities = Activity::all();
        return view('activities.index', compact('activities'));
    }

    public function list()
    {
        $activities = Activity::all();
        return $activities;
    }

    public function actFilter(ActivityFilters $filters)
    {
        return Activity::filter($filters)->get();
    }

    // public function actfilter(Request $request)
    // {
    // 	$activities = Activity::query();
    // 	if($request->exists('start_at')){
    // 		$activities->where('start_at', $request->start_at);
    // 	}
    // 	if($request->has('title')){
    // 		$activities->where('title', $request->title);
    // 	}
    // 	return $activities->get();
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('activities.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attributes = request()->validate([
            'title' => ['required', 'min:3'],
            'start_at' => ['required'],
            'end_at' => [],
            'img_cover' => ['image', 'nullable'],
            'img_cover_old' => [],
            'note' => [],
        ]);
        $attributes['owner_id'] = Auth::id();
        if ($request->hasFile('img_cover')) {
            $salt = 'donate';
            $filename = md5($salt . uniqid());
            $extension = $request->file('img_cover')->extension();

            $save_path = storage_path("app/public/images/cover/{$filename}.{$extension}");
            $url_path = "storage/images/cover/{$filename}.{$extension}";

            $image = new SimpleImage();
            $image
                ->fromFile($request->file('img_cover'))
                ->autoOrient()
                ->resize(400, 0)
                ->toFile($save_path);

            // $path = $request->file('img_cover')->store('public/images/cover');

            $attributes['img_cover'] = $url_path;

        // $attributes['img_cover'] = "public/images/cover/{$filename}.{$extension}";
        } else {
            $attributes['img_cover'] = $request->has('img_cover_old') ? $request->input('img_cover_old') : "";
        }
        // dd($attributes['owner_id']);
        $activity = Activity::create($attributes);
        // return redirect('/activities');
        return ['success' => true];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function show(Activity $activity)
    {
        return $activity;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function edit(Activity $activity)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Activity $activity)
    {
        $attributes = request()->validate([
            'title' => ['required', 'min:3'],
            'start_at' => ['required'],
            'end_at' => [],
            'img_cover' => ['image', 'nullable'],
            'img_cover_old' => [],
            'note' => [],
        ]);
        $attributes['owner_id'] = Auth::id();

        if ($request->hasFile('img_cover')) {
            $salt = 'donate';
            $filename = md5($salt . uniqid());
            $extension = $request->file('img_cover')->extension();

            $save_path = storage_path("app/public/images/cover/{$filename}.{$extension}");
            $url_path = "storage/images/cover/{$filename}.{$extension}";

            $image = new SimpleImage();
            $image
                ->fromFile($request->file('img_cover'))
                ->autoOrient()
                ->resize(400, 0)
                ->toFile($save_path);

            // $path = $request->file('img_cover')->store('public/images/cover');

            $attributes['img_cover'] = $url_path;

        // $attributes['img_cover'] = "public/images/cover/{$filename}.{$extension}";
        } else {
            $attributes['img_cover'] = $request->has('img_cover_old') ? $request->input('img_cover_old') : "";
        }

        $activity->update($attributes);
        return $activity;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function destroy(Activity $activity)
    {
        $activity->delete();
        return ['success' => true];
    }
}
