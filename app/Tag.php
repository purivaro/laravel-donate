<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    public function activities() {
			//many to many
			return $this->belongsToMany(Activity::class)->withTimestamps();
		}
}
